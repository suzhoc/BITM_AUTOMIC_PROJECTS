<?php
include_once ($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."BITM_Atomic_Project".DIRECTORY_SEPARATOR."Views".DIRECTORY_SEPARATOR."startup.php");

use App\BITM\SEIP107348\Hobby\Hobby;
use App\BITM\SEIP107348\Utility\Utility;


$obj = new Hobby();
$obj = $obj->show($_GET['id']);
?>            
<html>
    <head>
        <title>HOBBIES</title>
        <link href="../../../Asset/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="../../../Asset/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="../../../Asset/css/main.css" rel="stylesheet" type="text/css"/>
        <style>
            .add_book_form{
                padding: 15px 170px;
            }
        </style>
    </head>
    <body>
        <section class="header_part">
            <div class="container">
                <div class="row">
                  <div>
                        <div class="col-md-2">
                            <p class="header_text text-success">Codechamps</p>
                        </div>
                        <div class="col-md-10">  					

                             <p class="navbar-text pull-right">
                                <a href="#"><span class="glyphicon glyphicon-hand-right"></span>  SEIP-107348, 107477, 107897, 107314</a>
                            </p>
                            <p class="navbar-text pull-right">
                                <a href="#"><span class="glyphicon glyphicon-user"></span> Code Champs</a>
                            </p>
                        </div>
                    </div>
            </div>
        </section>


        <!-- =============== navbar-section =============== -->
        <section class="table_section">
            <div class="container">
                <div class="row col-md-10 col-md-offset-1  custyle">
                    <div class="table_nav">
                        <nav class="navbar navbar-default" role="navigation">
                            <!-- Collect the nav links, forms, and other content for toggling -->
                            <div class="collapse navbar-collapse navbar-ex1-collapse">
                                <ul class="nav navbar-nav">
                                    <li><a href="../../../index.php">HOME</a></li>
                                    <li><a href="index.php">VIEW</a></li>
                                    <li><a href="create.php">ADD HOBBIES</a></li>
                                    <!--<li><a href="#">Link</a></li>-->
                                </ul>

                                <ul class="nav navbar-nav navbar-right"> 
                                    <form class="navbar-form navbar-left" role="search">
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="Search">
                                        </div>
                                        <button type="submit" class="btn btn-default">Submit</button>
                                    </form>

                                    <li><a href="#">Download PDF</a></li>

                                </ul>
                            </div><!-- /.navbar-collapse -->
                        </nav>
                    </div>

                    <!--======================= add-book-form =====================-->
                    
                    <div class="add_book">
                        <div class="add_book_form col-md-8">
                            <h4 class="color_orange"><?php echo $obj['name']; ?>'s Hobbies</h4>
                            <form class="" action="edit.php?id=<?php echo $obj['id']; ?>" method="post">
                                <div class="form-group">
                                    <input type ="hidden"
                                           value ="<?php echo $obj['id']; ?>"
                                           name ="id"
                                    >                                    
                                    <p class="form-control" id="exampleInputEmail1" ><?php echo $obj['hobby']; ?></p>
                                </div>

                                <button type="submit" name="submit" class="btn btn-default">Edit</button>
                            </form>

                        </div>
                    </div>
                    <div class="back_button">
                        <a href="index.php">
                            <button type="submit" class="btn btn-danger">&ll; Back</button>
                        </a>
                    </div>
                </div>
            </div>
        </section>
          <script src="https://code.jquery.com/jquery-2.1.4.min.js" type="text/javascript" ></script>
          <script>
              $("input").prop("disable", true); 
          </script>
    </body>
</html>